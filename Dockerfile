FROM ubuntu:20.04

ADD https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-a/8.3-2019.03/binrel/gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu.tar.xz /opt
ADD https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-a/9.2-2019.12/binrel/gcc-arm-9.2-2019.12-x86_64-aarch64-none-elf.tar.xz /opt

ENV DEBIAN_FRONTENV noninteractive

# Fix timezone issue
ENV TZ=Europe/London
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ARG user_id
ARG group_id

# Required Packages for:
# 1. the Host Development System
# 2. Freescale compilation
# 3. Additional host packages required by poky/scripts/wic
# 4. Development edition
# http://www.yoctoproject.org/docs/latest/mega-manual/mega-manual.html#required-packages-for-the-host-development-system

# Environment configuration:
# 1. Add "repo" tool (used by many Yocto-based projects)
# 2. Create a non-root user that will perform the actual build
# 3. Fix error "Please use a locale setting which supports utf-8."
#    See https://wiki.yoctoproject.org/wiki/TipsAndTricks/ResolvingLocaleIssues
# 4. clean useless packages, remove apt itself, man, locales and doc files.

RUN apt-get update && apt-get -y upgrade && \
    apt-get install -y --no-install-recommends \
        android-sdk-ext4-utils \
        android-sdk-libsparse-utils \
        android-tools-adb \
        android-tools-fastboot \
        android-tools-mkbootimg \
        bc \
        bison \
        build-essential \
        bzip2 \
        ca-certificates \
        chrpath \
        clang \
        cpio \
        curl \
        debianutils \
        device-tree-compiler \
        diffstat \
        dwarves \
        file \
        flex \
        gawk \
        gdisk \
        git-lfs \
        g++-7-multilib \
        g++-8-multilib \
        g++-9-multilib \
        g++-10-multilib \
        g++-multilib \
        libdw-dev \
        libelf-dev \
        libghc-gnutls-dev \
        liblz4-tool \
        liblzo2-2 \
        liblzo2-dev \
        liblz-dev \
        libncurses5 \
        libncurses5-dev \
        libssl-dev \
        libz-dev \
        locales \
        lz4 \
        lzop \
        m4 \
        mtd-utils \
        ninja-build \
        openjdk-8-jdk \
        openssh-client \
        python2 \
        python3-distutils \
        python3-virtualenv \
        python-is-python3 \
        rsync \
        rustfmt \
        sudo \
        swig \
        tar \
        texinfo \
        unzip \
        uuid \
        uuid-dev \
        u-boot-tools \
        wget \
        xxd \
        xz-utils \
        zip \
        zlib1g-dev \
        zstd \
    && \
    wget -qO- http://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo && \
        chmod a+x /usr/local/bin/repo \
    && \
        echo "Extracting gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu.tar.xz..." \
        && cd /opt \
        && tar xf gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu.tar.xz \
        && rm gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu.tar.xz \
    && \
        echo "Extracting gcc-arm-9.2-2019.12-x86_64-aarch64-none-elf.tar.xz..." \
        && cd /opt \
        && tar xf gcc-arm-9.2-2019.12-x86_64-aarch64-none-elf.tar.xz \
        && rm gcc-arm-9.2-2019.12-x86_64-aarch64-none-elf.tar.xz \
    && \
    getent group build 2>/dev/null || groupadd --gid ${group_id} build && \
        id build 2>/dev/null || useradd --uid ${user_id} --gid ${group_id} --create-home build && \
        echo "build ALL=(ALL) NOPASSWD: ALL" | tee -a /etc/sudoers \
    && \
    sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
        echo 'LANG="en_US.UTF-8"'>/etc/default/locale && \
        dpkg-reconfigure --frontend=noninteractive locales && \
        update-locale LANG=en_US.UTF-8 \
    && \
    apt-get clean && apt-get autoclean && \
        apt-get autoremove -y --allow-remove-essential apt && \
        rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
        rm -rf /var/cache/apt/archives && \
        rm -rf /usr/share/doc/ /usr/share/man/ /usr/share/locale \
    && \
    echo "Finish"

# Enable high priority builds
RUN echo "build - nice -20" | tee -a /etc/security/limits.conf

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

USER build
WORKDIR /home/build
CMD "/bin/bash"

# All builds will be done by user aosp
COPY --chown=build:build copy_files/git_config /home/build/.gitconfig
COPY --chown=build:build copy_files/ssh_config /home/build/.ssh/config
COPY --chown=build:build copy_files/inputrc /home/build/.inputrc

COPY --chown=build:build scripts/nxp_build.sh /home/build/nxp_build.sh
COPY --chown=build:build scripts/nxp_test_cts.sh /home/build/nxp_test_cts.sh
COPY --chown=build:build scripts/nxp_test_vts.sh /home/build/nxp_test_vts.sh

# EOF
