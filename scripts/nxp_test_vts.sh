#!/bin/bash

CPU_NUM=$(nproc)
SERIAL=''
TRIES_LIMIT=100
DEVICE='imx'
ADB_WAIT_DEVICE_TIMEOUT=60s

set -e

cd ~/aosp

if [ ! -d android_build ]
then
    echo "Build AOSP before run test"
    exit 1
fi

cd android_build

source build/envsetup.sh
lunch ${AOSP_PRODUCT_NAME}-${AOSP_TARGET_BUILD_VARIANT}

echo "Waiting ${ADB_WAIT_DEVICE_TIMEOUT} for any connected devices..."
timeout ${ADB_WAIT_DEVICE_TIMEOUT} adb wait-for-device
if [[ $? -ne 0 ]]
then
    echo "[ERROR] Devices not found!"
    exit 1
fi

echo "Get first device ${DEVICE} in list"
SERIAL=$(adb devices -l | grep -i ${DEVICE} | head -n1 | sed 's|[\t ].*||g')
if [ -z "${SERIAL}" ]
then
    echo "[ERROR] Device ${DEVICE} not found!"
    exit 1
else
    echo "[INFO] Found device ${DEVICE} with serial ${SERIAL}"
fi

# check if previously has device with serial
RESULT_LAST=$(echo exit | vts-tradefed list results 2>&1 | grep "\[${SERIAL}\]" | tail -n1)

if [ -z "${RESULT_LAST}" ]
then
    echo "[INFO] not found any VTS tests for device ${SERIAL}"
    echo "[INFO] run full VTS..."
    vts-tradefed \
        run commandAndExit \
        vts \
        --retry-strategy RETRY_ANY_FAILURE \
        --max-testcase-run-count 2
fi

tries=0
while [[ ${tries} -lt ${TRIES_LIMIT} ]]
do
    RESULT_LAST=$(echo exit | vts-tradefed list results 2>&1 | grep "\[${SERIAL}\]" | tail -n1)

    # parse last result
    idx=0
    for str in ${RESULT_LAST}
    do
        case $idx in
            0) SESSION_ID="$str";;
            3) MODULES_DONE="$str";;
            5) MODULES_TOTAL="$str";;
        esac
        let ++idx
    done

    echo "Last result: session_id = ${SESSION_ID}, modules ${MODULES_DONE} of ${MODULES_TOTAL}"

    # if all modules done
    if [[ ${MODULES_DONE} -eq ${MODULES_TOTAL} ]]
    then
        echo "[INFO] all modules done"
        echo "[INFO] repeat fail tests..."

        vts-tradefed \
            run commandAndExit \
            retry \
            --retry "${SESSION_ID}" \
            --retry-type FAILED

        RESULT_PRELAST=$(echo exit | vts-tradefed list results 2>&1 | grep "\[${SERIAL}\]" | tail -n2 | head -n1)
        RESULT_LAST=$(echo exit | vts-tradefed list results 2>&1 | grep "\[${SERIAL}\]" | tail -n1)

        # parse pre-last result
        idx=0
        for str in ${RESULT_PRELAST}
        do
            case $idx in
                1) RESULT_PRELAST_PASS="$str";;
                2) RESULT_PRELAST_FAIL="$str";;
            esac
            let ++idx
        done

        # parse last result
        idx=0
        for str in ${RESULT_LAST}
        do
            case $idx in
                1) RESULT_LAST_PASS="$str";;
                2) RESULT_LAST_FAIL="$str";;
            esac
            let ++idx
        done

        if [[ ${RESULT_PRELAST_PASS} -eq ${RESULT_LAST_PASS} \
              && \
              ${RESULT_PRELAST_FAIL} -eq ${RESULT_LAST_FAIL} ]]
        then
            echo "[INFO] Done with ${tries} tries"
            exit 0
        fi

    else
        echo "[INFO] not all modules done"
        echo "[INFO] retry not executed tests..."

        vts-tradefed \
            run commandAndExit \
            retry \
            --retry "${SESSION_ID}" \
            --retry-type NOT_EXECUTED
    fi
    let ++tries
done

if [[ ${tries} -ge ${TRIES_LIMIT} ]]
then
    echo "[ERROR] Too many tries. ${tries} / ${TRIES_LIMIT}"
    exit 1
fi