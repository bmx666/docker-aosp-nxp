#!/bin/bash

CPU_NUM=$(nproc)

set -e

cd ~/aosp

if [[ "${TARGET_MANIFEST_NAME}" =~ "trusty" ]]
then
    mkdir -p trusty_build && cd trusty_build
else
    mkdir -p android_build && cd android_build
fi

echo "Initialize manifest of repositories..."
${BUILD_PRIO}\
repo init \
    --submodules \
    --manifest-url=${TARGET_MANIFEST_URL} \
    --manifest-branch=${TARGET_MANIFEST_BRANCH} \
    --manifest-name="${TARGET_MANIFEST_NAME}.xml" \
    --quiet
echo "Manifest is successful initialized"

echo "Fetching repositories..."
${BUILD_PRIO}\
repo sync \
    --retry-fetches=99 \
    --force-sync \
    --force-remove-dirty \
    --prune \
    --network-only \
    --jobs=${CPU_NUM} \
    --current-branch \
    --no-tags \
    --quiet
echo "Repositories are successful fetched"

echo "Synchronizing repositories..."
if [[ "${TARGET_MANIFEST}" =~ "trusty" ]]
then
    ${BUILD_PRIO}\
    repo sync \
        --force-remove-dirty \
        --local-only \
        --jobs=${CPU_NUM} \
        --quiet
else
    ${BUILD_PRIO}\
    repo sync \
        --force-remove-dirty \
        --detach \
        --local-only \
        --jobs=${CPU_NUM} \
        --quiet
fi
echo "Repositories are successful synchronized"

if [[ "${TARGET_MANIFEST_NAME}" =~ "trusty" ]]
then
    TRUSTY_PRODUCT_NAME_ALIAS="${TRUSTY_PRODUCT_NAME}"
    if [[ "${TRUSTY_PRODUCT_NAME}" =~ "imx8mq" ]]
    then
        TRUSTY_PRODUCT_NAME_ALIAS="imx8m"
    fi
    rm -fr build-${TRUSTY_PRODUCT_NAME_ALIAS}
    source trusty/vendor/google/aosp/scripts/envsetup.sh
    BUILDROOT="$(pwd)" \
    ${BUILD_PRIO}\
    make -j${CPU_NUM} ${TRUSTY_PRODUCT_NAME_ALIAS} 2>&1 | tee build-log.txt
    #cp build-${TRUSTY_PRODUCT_NAME_ALIAS}/lk.bin \
    #    ~/android_build/vendor/nxp/fsl-proprietary/uboot-firmware/imx8m/custom-tee-${TRUSTY_PRODUCT_NAME}.bin
else
    ${BUILD_PRIO}\
    bash -c "source build/envsetup.sh && \
             lunch ${AOSP_PRODUCT_NAME}-${AOSP_TARGET_BUILD_VARIANT} && \
             ${AOSP_BUILD_SCRIPT} -j${CPU_NUM} 2>&1 | tee build-log.txt && \
             make vts cts -j${CPU_NUM} 2>&1 | tee build-log-tests.txt && \
             echo DONE || echo FAIL"
fi
