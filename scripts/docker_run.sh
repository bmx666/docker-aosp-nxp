#!/bin/bash

if [ -z "$DOCKER_COMMAND" ]
then
	echo "env DOCKER_COMMAND is not set"
	echo -n "Continue? [y/n/?]: "
	read -n1 CHOOSE_YES_NO
	echo ""

	if [[ "${CHOOSE_YES_NO}" =~ ^(y|Y)$ ]]
	then
		echo "Run docker without command"
	else
		echo "Abort"
		exit 1
	fi
	unset CHOOSE_YES_NO
fi

RETVAL=''

function set_choices() {
	LIST_=${1}
	ENV_NAME=${2}
	NAME=${3}
	echo "env ${ENV_NAME} is not set"
	if [[ "${#LIST_[@]}" -eq 1 ]]
	then
		RETVAL="${LIST_[0]}"
		unset LIST_
		if [ "${RETVAL}" != "custom" ]
		then
			echo "autoset ${ENV_NAME} = '${RETVAL}'"
			return
		fi
	else
		echo "Choose ${NAME}:"
		for i in $(seq 1 ${#LIST_[@]})
		do
			echo "${i}. ${LIST_[$((${i}-1))]}"
		done
		echo -n "Enter ${NAME} (1-${#LIST_[@]}): "
		LIST_COUNT=${#LIST_[@]}
		read -n${#LIST_COUNT} CHOOSE_
		unset LIST_COUNT
		echo ""

		if [[ "${CHOOSE_}" -ge 1 && "${CHOOSE_}" -le ${#LIST_[@]} ]]
		then
			RETVAL="${LIST_[$((${CHOOSE_}-1))]}"
			unset LIST_
			unset CHOOSE_
		else
			unset LIST_
			unset CHOOSE_
			echo "Wrong choise"
			exit 1
		fi
	fi

	if [ "${RETVAL}" == "custom" ]
	then
		echo "env ${ENV_NAME} is 'custom'"
		echo "better export env ${ENV_NAME} for future use"
		echo -n "Set ${NAME}: "
		read CHOOSE_NAME

		# remove leading whitespace characters
		CHOOSE_NAME="${CHOOSE_NAME#"${CHOOSE_NAME%%[![:space:]]*}"}"
		# remove trailing whitespace characters
		CHOOSE_NAME="${CHOOSE_NAME%"${CHOOSE_NAME##*[![:space:]]}"}"

		# check on whitespaces inside
		if [[ "${CHOOSE_NAME}" =~ "[[:space:]]+" ]]
		then
			echo "env ${ENV_NAME} has whitespaces inside"
			exit 1
		fi

		if [ -z "${CHOOSE_NAME}" ]
		then
			echo "env ${ENV_NAME} is empty, set 'custom' by default"
			CHOOSE_NAME="custom"
		fi

		RETVAL="${CHOOSE_NAME}"
		unset CHOOSE_NAME
	fi
}

if [ -z "${BUILD_PRIO}" ]
then
	BUILD_PRIO=""
	echo -n "Choose high/low priority for build or skip [h/l/*]: "
	read -n1 CHOOSE_BUILD_PRIO
	echo ""

	if [[ "${CHOOSE_BUILD_PRIO}" =~ ^(h|H)$ ]]
	then
		echo "Using high priority for build"
		BUILD_PRIO=" ionice -c 2 -n 0 nice -n -20 "
		NICE_PRIO_ARGS="--cap-add=SYS_NICE --ulimit nice=40"
	elif [[ "${CHOOSE_BUILD_PRIO}" =~ ^(l|L)$ ]]
	then
		echo "Using low priority for build"
		BUILD_PRIO=" ionice -c 3 nice -n 19 "
	fi
	unset CHOOSE_BUILD_PRIO
fi

if [ -z "${WORKDIR}" ]
then
	echo "env WORKDIR is not set"
	echo -n "Set working directory: "
	read CHOOSE_WORKDIR
	if [ ! -d "${CHOOSE_WORKDIR}" ]
	then
		echo "Directory '${WORKDIR}' is not exists"
		exit 1
	fi
	WORKDIR="${CHOOSE_WORKDIR}"
	unset CHOOSE_WORKDIR
fi

if [ -z "${TARGET_MANIFEST_URL}" ]
then
	LIST_=(\
		"custom"\
		"https://github.com/nxp-imx/imx-manifest"\
	)
	set_choices ${LIST_} "TARGET_MANIFEST_URL" "target manifest url"
	unset LIST_
	TARGET_MANIFEST_URL=${RETVAL}
fi

if [ -z "${TARGET_MANIFEST_BRANCH}" ]
then
	LIST_=(\
		"custom"\
		"imx-android-10"\
		"imx-android-11"\
		"imx-android-12"\
		"imx-android-13"\
	)
	set_choices ${LIST_} "TARGET_MANIFEST_BRANCH" "target manifest branch"
	unset LIST_
	TARGET_MANIFEST_BRANCH=${RETVAL}
fi

if [ -z "${TARGET_MANIFEST_NAME}" ]
then
	case "${TARGET_MANIFEST_BRANCH}" in
		"imx-android-10")
			LIST_=(\
				"imx-android-10.0.0_1.0.0_github"\
				"imx-android-10.0.0_2.0.0_github"\
				"imx-android-10.0.0_2.1.0_github"\
				"imx-android-10.0.0_2.3.0_github"\
				"imx-android-10.0.0_2.5.0_github"\
				"imx-android-10.0.0_2.6.0_github"\
				\
				"imx-automotive-10.0.0_1.1.0"\
				"imx-automotive-10.0.0_2.2.0"\
				"imx-automotive-10.0.0_2.4.0"\
				\
				"imx-trusty-android-10.0.0_1.0.0_github"\
				"imx-trusty-android-10.0.0_2.0.0_github"\
				"imx-trusty-android-10.0.0_2.1.0_github"\
				"imx-trusty-android-10.0.0_2.3.0_github"\
				"imx-trusty-android-10.0.0_2.5.0_github"\
				"imx-trusty-android-10.0.0_2.6.0_github"\
				\
				"imx-trusty-automotive-10.0.0_1.1.0"\
				"imx-trusty-automotive-10.0.0_2.2.0"\
				"imx-trusty-automotive-10.0.0_2.4.0"\
			)
			;;
		"imx-android-11")
			LIST_=(\
				"imx-android-11.0.0_1.0.0_github"\
				"imx-android-11.0.0_1.2.0_github"\
				"imx-android-11.0.0_1.2.1_github"\
				"imx-android-11.0.0_2.0.0_github"\
				"imx-android-11.0.0_2.2.0_github"\
				"imx-android-11.0.0_2.4.0_github"\
				"imx-android-11.0.0_2.6.0_github"\
				\
				"imx-automotive-11.0.0_1.1.0"\
				"imx-automotive-11.0.0_2.1.0"\
				"imx-automotive-11.0.0_2.3.0"\
				"imx-automotive-11.0.0_2.5.0"\
				\
				"imx-trusty-android-11.0.0_1.0.0_github"\
				"imx-trusty-android-11.0.0_1.2.0_github"\
				"imx-trusty-android-11.0.0_1.2.1_github"\
				"imx-trusty-android-11.0.0_2.0.0_github"\
				"imx-trusty-android-11.0.0_2.2.0_github"\
				"imx-trusty-android-11.0.0_2.4.0_github"\
				"imx-trusty-android-11.0.0_2.6.0_github"\
				\
				"imx-trusty-automotive-11.0.0_1.1.0"\
				"imx-trusty-automotive-11.0.0_2.1.0"\
				"imx-trusty-automotive-11.0.0_2.3.0"\
				"imx-trusty-automotive-11.0.0_2.5.0"\
			)
			;;
		"imx-android-12")
			LIST_=(\
				"imx-android-12.0.0_1.0.0_github"\
				"imx-android-12.0.0_2.0.0_github"\
				"imx-android-12.1.0_1.0.0"\
				\
				"imx-automotive-12.0.0_1.1.0"\
				"imx-automotive-12.0.0_2.1.0"\
				"imx-automotive-12.1.0_1.1.0"\
				\
				"imx-trusty-android-12.0.0_1.0.0_github"\
				"imx-trusty-android-12.0.0_2.0.0_github"\
				"imx-trusty-android-12.1.0_1.0.0"\
				\
				"imx-trusty-automotive-12.0.0_1.1.0"\
				"imx-trusty-automotive-12.0.0_2.1.0"\
				"imx-trusty-automotive-12.1.0_1.1.0"\
			)
			;;
		"imx-android-13")
			LIST_=(\
				"imx-android-13.0.0_1.0.0"\
				"imx-android-13.0.0_1.2.0"\
				"imx-android-13.0.0_2.0.0"\
				\
				"imx-automotive-13.0.0_1.1.0"\
				"imx-automotive-13.0.0_1.3.0"\
				\
				"imx-trusty-android-13.0.0_1.0.0"\
				"imx-trusty-android-13.0.0_1.2.0"\
				"imx-trusty-android-13.0.0_2.0.0"\
				\
				"imx-trusty-automotive-13.0.0_1.1.0"\
				"imx-trusty-automotive-13.0.0_1.3.0"\
			)
			;;
		"custom" | *)
			LIST_=("custom");;
	esac
	set_choices ${LIST_} "TARGET_MANIFEST_NAME" "target manifest name"
	unset LIST_
	TARGET_MANIFEST_NAME=${RETVAL}
fi

if [[ "${TARGET_MANIFEST_NAME}" =~ "trusty" ]]
then
	if [ -z "${TRUSTY_PRODUCT_NAME}" ]
	then
		LIST_=(\
			"imx8mq"\
			"imx8mm"\
			"imx8mn"\
			"imx8mp"\
			"imx8qm"\
			"imx8qxp"\
			"imx8ulp"\
		)
		set_choices ${LIST_} "TRUSTY_PRODUCT_NAME" "TrustyOS product name"
		unset LIST_
		TRUSTY_PRODUCT_NAME=${RETVAL}
	fi
else
	if [ -z "${AOSP_BUILD_SCRIPT}" ]
	then
		AOSP_BUILD_SCRIPT="./imx-make.sh"
	fi

	if [ -z "${AOSP_PRODUCT_NAME}" ]
	then
		LIST_=(\
			"sabresd_6dq"\
			"sabresd_6dq_car"\
			"sabreauto_6q"\
			"evk_6sl"\
			"sabresd_6sx"\
			"sabresd_7d"\
			"evk_7ulp"\
			"evk_7ulp_revb"\
			"evk_8mq"\
			"evk_8mq_drm"\
			"evk_8mm"\
			"evk_8mm_drm"\
			"evk_8mn"\
			"evk_8mp"\
			"evk_8ulp"\
			"mek_8q"\
			"mek_8q_car"\
			"mek_8q_car2"\
		)
		set_choices ${LIST_} "AOSP_PRODUCT_NAME" "AOSP product name"
		unset LIST_
		AOSP_PRODUCT_NAME=${RETVAL}
	fi

	if [ -z "${AOSP_TARGET_BUILD_VARIANT}" ]
	then
		LIST_=(\
			"user"\
			"userdebug"\
		)
		set_choices ${LIST_} "AOSP_TARGET_BUILD_VARIANT" "AOSP build variant"
		unset LIST_
		AOSP_TARGET_BUILD_VARIANT=${RETVAL}
	fi
fi

if [ -z "${AARCH64_GCC_CROSS_COMPILE}" ]
then
	AARCH64_GCC_CROSS_COMPILE_8_3_2019_03="/opt/gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu/bin/aarch64-linux-gnu-"
	AARCH64_GCC_CROSS_COMPILE_9_2_2019_12="/opt/gcc-arm-9.2-2019.12-x86_64-aarch64-none-elf/bin/aarch64-none-elf-"

	if [[ "${TARGET_MANIFEST_NAME}" =~ "android-10" ]]; then
		AARCH64_GCC_CROSS_COMPILE="${AARCH64_GCC_CROSS_COMPILE_8_3_2019_03}"
	elif [[ "${TARGET_MANIFEST_NAME}" =~ "android-11" ]]; then
		AARCH64_GCC_CROSS_COMPILE="${AARCH64_GCC_CROSS_COMPILE_8_3_2019_03}"
	elif [[ "${TARGET_MANIFEST_NAME}" =~ "android-12.0" ]]; then
		AARCH64_GCC_CROSS_COMPILE="${AARCH64_GCC_CROSS_COMPILE_8_3_2019_03}"
	elif [[ "${TARGET_MANIFEST_NAME}" =~ "android-12.1" ]]; then
		AARCH64_GCC_CROSS_COMPILE="${AARCH64_GCC_CROSS_COMPILE_9_2_2019_12}"
	elif [[ "${TARGET_MANIFEST_NAME}" =~ "android-13" ]]; then
		AARCH64_GCC_CROSS_COMPILE="${AARCH64_GCC_CROSS_COMPILE_9_2_2019_12}"
	else
		echo "Please set AARCH64_GCC_CROSS_COMPILE according Android User's Guide"
		exit 1
	fi
fi

# Set uid and gid to match host current user as long as NOT root
if [ $(id -u) -ne "0" ]
then
	HOST_ID_ARGS="--env USER_ID=$(id -u) --env GROUP_ID=$(id -g)"
else
	echo "Compilation under 'root' not supported"
	exit 1
fi

if [ ! -S "$SSH_AUTH_SOCK" ]
then
	# Kill ssh-agent
	trap "ssh-agent -k" exit
	eval "$(ssh-agent -s)"
	if [ -n "$SSH_PRIVATE_KEY_PATH" ]
	then
		if [ -f "$SSH_PRIVATE_KEY_PATH" ]
		then
			ssh-add "$SSH_PRIVATE_KEY_PATH"
		else
			echo "[WARNING] SSH_PRIVATE_KEY_PATH: '$SSH_PRIVATE_KEY_PATH' not found"
			exit 1
		fi
	else
		# add default keys
		if [ -f ~/.ssh/id_rsa ]
		then
			ssh-add ~/.ssh/id_rsa
		fi
		if [ -f ~/.ssh/id_ed25519 ]
		then
			ssh-add ~/.ssh/id_ed25519
		fi
	fi
fi

if [ -S "$SSH_AUTH_SOCK" ]
then
	SSH_AUTH_ARGS="--volume ${SSH_AUTH_SOCK}:/ssh-agent --env SSH_AUTH_SOCK=/ssh-agent"
else
	echo "ssh-agent auth socket not found"
	exit 1
fi

echo "Starting Docker..."

if [ -z "$DOCKER_COMMAND" ]
then
	DOCKER_TTY="--interactive --tty"
fi

if [ -n "$DOCKER_NAME" ]
then
	DOCKER_NAME="--name=${DOCKER_NAME}"
fi

echo "passing USB for ADB into docker container"
USB_PASS_ARGS="--privileged -v /dev/bus/usb:/dev/bus/usb"

docker run \
	--rm \
	${DOCKER_EXTRA_FLAGS} \
	${USB_PASS_ARGS} \
	${DOCKER_TTY} \
	${DOCKER_NAME} \
	${HOST_ID_ARGS} \
	${SSH_AUTH_ARGS} \
	${NICE_PRIO_ARGS} \
	--env BUILD_PRIO="${BUILD_PRIO}" \
	--env TRUSTY_PRODUCT_NAME="${TRUSTY_PRODUCT_NAME}" \
	--env AOSP_BUILD_SCRIPT="${AOSP_BUILD_SCRIPT}" \
	--env AOSP_PRODUCT_NAME="${AOSP_PRODUCT_NAME}" \
	--env AOSP_TARGET_BUILD_VARIANT="${AOSP_TARGET_BUILD_VARIANT}" \
	--env TARGET_MANIFEST_URL="${TARGET_MANIFEST_URL}" \
	--env TARGET_MANIFEST_BRANCH="${TARGET_MANIFEST_BRANCH}" \
	--env TARGET_MANIFEST_NAME="${TARGET_MANIFEST_NAME}" \
	--env AARCH64_GCC_CROSS_COMPILE="${AARCH64_GCC_CROSS_COMPILE}" \
	--volume="${WORKDIR}":/home/build/aosp \
	${DOCKER_IMAGE} \
	${DOCKER_COMMAND}
